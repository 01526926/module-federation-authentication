import {Component, NgZone, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthenticationService} from '../service/authentication.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: any;
  loading = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private zone: NgZone) {
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    AuthenticationService.registerLogoutListener();
  }

  onLogin() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    setTimeout(() => {
      AuthenticationService.login({username: this.f.username.value, password: this.f.password.value});
      this.loading = false;
      this.zone.run(() => this.router.navigate(['/dashboard']));
    }, 1000);
  }
}
