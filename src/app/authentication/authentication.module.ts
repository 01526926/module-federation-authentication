import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './authentication/login.component';
import {RouterModule} from '@angular/router';
import {AUTHENTICATION_ROUTES} from './authentication.routes';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(AUTHENTICATION_ROUTES),
    FlexLayoutModule,
    ReactiveFormsModule
  ]
})
export class AuthenticationModule {
}
