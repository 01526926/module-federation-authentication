import {Injectable} from '@angular/core';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  static login(user: User) {
    const channel = new BroadcastChannel("currentUser");
    channel.postMessage(user.username);
  }

  static registerLogoutListener() {
    const authChannel = new BroadcastChannel("authentication");
    authChannel.onmessage = (e) => {
      if (e.data === "logout") {
        const userChannel = new BroadcastChannel("currentUser");
        userChannel.postMessage(undefined);
      }
    };
  }
}
