import {Routes} from '@angular/router';
import {LoginComponent} from './authentication/login.component';

export const AUTHENTICATION_ROUTES: Routes = [
  {
    path: 'login',
    component: LoginComponent
  }
]
